package com.example.chessassignment.chess.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.chessassignment.R;
import com.example.chessassignment.chess.Utils.AppConstants;

public class BoardAdapter extends BaseAdapter {

    private Context mContext;

    public BoardAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return AppConstants.NUM_CLOUMNS * AppConstants.NUM_ROWS;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        FrameLayout mBoardCellLayout;
        TextView mHorseTextView;

        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.board_cell, null);
            mBoardCellLayout = view.findViewById(R.id.fl_board_cell_layout);
            mHorseTextView = view.findViewById(R.id.tv_horse);
           // mHorseTextView.setText(Integer.toString(i));

            int row = i / 8;
            int col = i % 8;

            if (((row % 2 == 0 && col % 2 == 0) || (row % 2 == 1 && col % 2 == 1))) {
                mBoardCellLayout.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            }
        }


        return view;
    }
}
