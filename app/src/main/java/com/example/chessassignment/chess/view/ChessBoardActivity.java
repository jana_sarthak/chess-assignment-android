package com.example.chessassignment.chess.view;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.example.chessassignment.R;
import com.example.chessassignment.chess.Utils.CoordinatePositionConversion;
import com.example.chessassignment.chess.view.adapter.BoardAdapter;

import java.util.ArrayList;

public class ChessBoardActivity extends AppCompatActivity {

    public static final int START_POS = 0;

    private GridView mChessboardGridView;
    private Button mStartButton, mNextButton, mPreviousButton, mChangeButton, mStopButton;
    private boolean isLastMoveBacktracked;
    private int mLastPosition, mCurrentPosition;
    private int currentPossibleMoves;
    private ArrayList<Point> mPossibleMoves;
    private BoardAdapter boardAdapter;
    private int currentMove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board_chess);
        initView();
        setBoardAdapter();
        setOnClickListeners();
    }

    private void setOnClickListeners() {

        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initialMove();
                mStartButton.setVisibility(View.GONE);
                mNextButton.setVisibility(View.VISIBLE);
                mStopButton.setVisibility(View.VISIBLE);
            }
        });

        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextMove();
                if (mPreviousButton.getVisibility() == View.INVISIBLE) {
                    mPreviousButton.setVisibility(View.VISIBLE);
                }
            }
        });

        mPreviousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isLastMoveBacktracked) {
                    isLastMoveBacktracked = true;
                    mPreviousButton.setVisibility(View.INVISIBLE);
                    restoreLastCoordinate(CoordinatePositionConversion.positionToCoordinate(mCurrentPosition));
                    move(CoordinatePositionConversion.positionToCoordinate(mLastPosition));
                }
            }
        });

        mChangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPossibleMoves != null && (currentMove < (currentPossibleMoves - 1))) {
                    changeMoves();
                } else {
                    Toast.makeText(ChessBoardActivity.this, "No more possible moves", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNextButton.setVisibility(View.INVISIBLE);
                mPreviousButton.setVisibility(View.INVISIBLE);
                mChangeButton.setVisibility(View.INVISIBLE);
                mStopButton.setVisibility(View.INVISIBLE);
                mStartButton.setVisibility(View.VISIBLE);
                boardAdapter.notifyDataSetChanged();
                mChessboardGridView.setAdapter(boardAdapter);
            }
        });
    }

    private void changeMoves() {
        currentMove++;
        move(mPossibleMoves.get(currentMove));
        restoreLastCoordinate(CoordinatePositionConversion.positionToCoordinate(mLastPosition));
    }

    private void nextMove() {
        //mLastPosition = mCurrentPosition;
        isLastMoveBacktracked = false;
        mPreviousButton.setVisibility(View.VISIBLE);
        restoreLastCoordinate(CoordinatePositionConversion.positionToCoordinate(mCurrentPosition));
        mPossibleMoves = CoordinatePositionConversion.possiblePoints(mCurrentPosition);
        mPossibleMoves.remove(getLastPosition());
        currentPossibleMoves = mPossibleMoves.size();
        if (mPossibleMoves.size() > 1) {
            if (mChangeButton.getVisibility() != View.VISIBLE) {
                mChangeButton.setVisibility(View.VISIBLE);
            }
        } else {
            mChangeButton.setVisibility(View.INVISIBLE);
        }
        move(mPossibleMoves.get(0));
        currentMove = 0;
    }

    private Point getLastPosition() {
        for (Point coordinate : mPossibleMoves) {
            if (CoordinatePositionConversion.coordinateToPosition(coordinate) == mLastPosition) {
                return coordinate;
            }
        }
        return null;
    }

    private void restoreLastCoordinate(Point point) {
        View view = (View) mChessboardGridView.getChildAt(CoordinatePositionConversion.coordinateToPosition(point));
        view.findViewById(R.id.tv_horse).setVisibility(View.INVISIBLE);
    }

    private void move(Point nextCoordinate) {
        View view = (View) mChessboardGridView.getChildAt(CoordinatePositionConversion.coordinateToPosition(nextCoordinate));
        view.findViewById(R.id.tv_horse).setVisibility(View.VISIBLE);
        mLastPosition = mCurrentPosition;
        mCurrentPosition = CoordinatePositionConversion.coordinateToPosition(nextCoordinate);
    }

    private void initialMove() {
        View view = (View) mChessboardGridView.getChildAt(START_POS);
        view.findViewById(R.id.tv_horse).setVisibility(View.VISIBLE);
        mLastPosition = mCurrentPosition = START_POS;
    }

    private void setBoardAdapter() {
        boardAdapter = new BoardAdapter(this);
        mChessboardGridView.setAdapter(boardAdapter);
    }

    private void initView() {
        mChessboardGridView = findViewById(R.id.gv_board);
        mChessboardGridView.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
        mStartButton = findViewById(R.id.btn_start);
        mNextButton = findViewById(R.id.btn_next);
        mPreviousButton = findViewById(R.id.btn_previous);
        mChangeButton = findViewById(R.id.btn_change);
        mStopButton = findViewById(R.id.btn_stop);
        mPossibleMoves = new ArrayList<>();
        isLastMoveBacktracked = false;
    }
}
