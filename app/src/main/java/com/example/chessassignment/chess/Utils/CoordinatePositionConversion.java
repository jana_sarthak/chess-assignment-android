package com.example.chessassignment.chess.Utils;

import android.graphics.Point;

import java.util.ArrayList;
import java.util.HashSet;

public final class CoordinatePositionConversion {

    public static final Point positionToCoordinate(int pos) {
        return new Point(pos/8, pos%8);
    }

    public static final int coordinateToPosition(Point coordinate) {
        int row = coordinate.x;
        int column = coordinate.y;

        return (row * 8 + column);
    }

    public static final ArrayList<Point> possiblePoints(int pos){
        Point mLastCoordinate = positionToCoordinate(pos);

        ArrayList<Point> possibleMoves = new ArrayList<>();

        // Down-Right movements

        if (withinLowerBound(mLastCoordinate, 2) && withinRightBound(mLastCoordinate, 1)) {
            Point mPossibleMove1 = new Point(mLastCoordinate.x + 2, mLastCoordinate.y + 1);
            possibleMoves.add(mPossibleMove1);
        }

        //Down-Right movements

        if (withinLowerBound(mLastCoordinate, 1) && withinRightBound(mLastCoordinate, 2)) {
            Point mPossibleMove5 = new Point(mLastCoordinate.x + 1, mLastCoordinate.y + 2);
            possibleMoves.add(mPossibleMove5);
        }

        //Down-Left movements
        if (withinLowerBound(mLastCoordinate, 2) && withinLeftBound(mLastCoordinate, 1)) {
            Point mPossibleMove2 = new Point(mLastCoordinate.x + 2, mLastCoordinate.y - 1);
            possibleMoves.add(mPossibleMove2);
        }

        //Up-right movements

        if (withinUpperBound(mLastCoordinate, 2) && withinRightBound(mLastCoordinate, 1)) {
            Point mPossibleMove3 = new Point(mLastCoordinate.x - 2, mLastCoordinate.y + 1);
            possibleMoves.add(mPossibleMove3);
        }

        //Up-Left movement
        if (withinUpperBound(mLastCoordinate, 2) && withinLeftBound(mLastCoordinate, 1)) {
            Point mPossibleMove4 = new Point(mLastCoordinate.x - 2, mLastCoordinate.y - 1);
            possibleMoves.add(mPossibleMove4);
        }


        //Up-Right movements
        if (withinUpperBound(mLastCoordinate, 1) && withinRightBound(mLastCoordinate, 2)) {
            Point mPossibleMove6 = new Point(mLastCoordinate.x - 1, mLastCoordinate.y + 2);
            possibleMoves.add(mPossibleMove6);
        }

        //Down-Left movements
        if (withinLowerBound(mLastCoordinate, 1) && withinLeftBound(mLastCoordinate, 2)) {
            Point mPossibleMove7 = new Point(mLastCoordinate.x + 1, mLastCoordinate.y - 2);
            possibleMoves.add(mPossibleMove7);
        }

        if (withinUpperBound(mLastCoordinate, 1) && withinLeftBound(mLastCoordinate, 2)) {
            Point mPossibleMove8 = new Point(mLastCoordinate.x - 1, mLastCoordinate.y - 2);
            possibleMoves.add(mPossibleMove8);
        }

        return possibleMoves;
    }

    private static boolean withinLowerBound(Point coordinate, int delMove) {
        return (coordinate.x + delMove) <= 7;
    }

    private static boolean withinUpperBound(Point coordinate, int delMove) {
        return (coordinate.x - delMove) >= 0;
    }

    private static boolean withinLeftBound(Point coordinate, int delMove) {
        return (coordinate.y - delMove) >= 0;
    }

    private static boolean withinRightBound(Point coordinate, int delMove) {
        return (coordinate.y + delMove) <= 7;
    }
}
